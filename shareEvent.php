<?php
if(empty($_SESSION))
{
	session_start();
}
include 'function.php';

header("Content-Type: application/json");
if(isset($_POST['shared_event_id']) and isset($_POST['shared_user_id'])){
	$event_id = $_POST['shared_event_id'];
	$user_id = $_POST['shared_user_id'];
}
else{
	echo json_encode(array(
		"success"=>false,
		"message"=>"please specify user and event"
		));
	exit;
}
?>